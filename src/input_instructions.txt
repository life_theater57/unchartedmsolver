COMPONENTMENU
Indicate units to add as presented below.

      'u' x y     where a Unit is added at position (x,y)
      's' x y e   where Spikes is added at position (x,y) with state e
                        e : 'w' - white, 'b' - black, 's' - spikes
      'b' x y     where a Button is added at position (x,y)
      'p' x y     where a PushTile is added at position (x,y)
      't' x y b   where a Target is added at position (x,y) with b a boolean
                        b : 't' - is built, 'f' - isn't built
      'c' x y     where a Cracked unit is added at position (x,y)
      'B' x y b   where a Block is added at position (x,y) with b a boolean
                        b : 't' - can be cross, 'f' - cannot be cross
      'T' x y b d where a Turret is added at position (x,y) with b a boolean and
                     d some char delimited by space
                        b : 't' - rotate, 'f' - don't rotate
                        d : 1 to 4 directions among following
                              'N' - north
                              'E' - east
                              'S' - south
                              'W' - West
      
 Enter 'r' x y to remove the unit added at position (x,y).
 Enter '.' to end units entry.
 Enter 'l' to see a list of units on the map.
 Enter 'm' to see this menu again.
 
-------------------------------------------------------------------------------------------------

COMPONENTSHORT
Enter '.' to end units entry.
 Enter 'l' to see a list of units on the map.
 Enter 'm' to see the component menu again.

-------------------------------------------------------------------------------------------------

CARRIABLEOBJMENU
Indicate Carriable objects to add as presented below.

      'c' x y     where a Cube is added at position (x,y)
      't' x y     where a TNT is added at position (x,y)
      'p' x y     where a round Plate is added at position (x,y)
      
      Enter '.' to end units entry.
          
-------------------------------------------------------------------------------------------------
      
ENDMENU      
Where is the End? Use format below.

      x y r       where the End is added at position (x,y) with r some position
                     required to be active to access the End
                        r : x1 y1 x2 y2 ... xn yn where xi yi is the position of
                              the ith component required to be active to access the End
                              
      
