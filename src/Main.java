import action.Action;
import map.*;

import java.lang.Character;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    private static ArrayList<Map> maps = new ArrayList<Map>();
    private final static String COMPONENTMENU =
            "\nIndicate units to add as presented below.\n\n"
                    + "       'u' x y     where a Unit is added at position (x,y)\n"
                    + "       's' x y e   where Spikes is added at position (x,y) with state e\n"
                    + "                         e : 'w' - white, 'b' - black, 's' - spikes\n"
                    + "       'b' x y     where a Button is added at position (x,y)\n"
                    + "       'p' x y     where a PushTile is added at position (x,y)\n"
                    + "       't' x y b   where a Target is added at position (x,y) with b a boolean\n"
                    + "                         b : 't' - is built, 'f' - isn't built\n"
                    + "       'c' x y     where a Cracked unit is added at position (x,y)\n"
                    + "       'B' x y b   where a Block is added at position (x,y) with b a boolean\n"
                    + "                         b : 't' - can be cross, 'f' - cannot be cross\n"
                    + "       'T' x y b d where a Turret is added at position (x,y) with b a boolean\n"
                    + "                      and d some char delimited by space\n"
                    + "                         b : 't' - rotate, 'f' - don't rotate\n"
                    + "                         d : 1 to 4 directions among following\n"
                    + "                               'N' - north\n" + "                               'E' - east\n"
                    + "                               'S' - south\n" + "                               'W' - West\n\n"
                    + " Enter 'r' x y to remove the unit added at position (x,y).\n"
                    + " Enter '.' to end units entry.\n"
                    + " Enter 'l' to see a list of units on the map.\n"
                    + " Enter 'm' to see this menu again.\n";
    private final static String COMPONENTSHORT =
            " Enter '.' to end units entry.\n"
                    + " Enter 'l' to see a list of units on the map.\n"
                    + " Enter 'm' to see the component menu again.\n";

    private final static String MAPSTARTMENU =
            "Where is the Start? Use format below.\n\n"
                    + "       x y         where the Start is added at position (x,y)\n";

    private final static String MAPENDMENU =
            "Where is the End? Use format below.\n\n"
                    + "       x y r       where the End is added at position (x,y) with r some position\n"
                    + "                      required to be active to access the End\n"
                    + "                         r : x1 y1 x2 y2 ... xn yn where xi yi is the position of\n"
                    + "                               the ith component required to be active to access the End\n";


    public static void main(String[] args) {
        maps.add(addMap());
        System.out.println(maps.get(0).getList());
        LinkedList<Action> solution = maps.get(0).resolve();
        for (Action action : solution) {
            System.out.println(action);
        }
    }

    /**
     * This method builds a Map from user entries.
     *
     * @return the Map built
     */
    private static Map addMap() {
        Scanner scan = new Scanner(System.in);

        System.out.println("\nWhat is the height of the field?");
        int height = scan.nextInt();

        System.out.println("How many try maximum?");
        int maxMoves = scan.nextInt();

        Map map = new Map(5, height, maxMoves); // width is always 5 and height is supposed to be less than 10
        System.out.println(COMPONENTMENU);

        scan.nextLine();
        String nextEntry = scan.nextLine();

        while (nextEntry.charAt(0) != '.') {
            if (nextEntry.length() > 4) {
                int x = Character.getNumericValue(nextEntry.charAt(2));
                int y = Character.getNumericValue(nextEntry.charAt(4));
                switch (nextEntry.charAt(0)) {
                    case 'u':
                        map.add(x, y, new Unit());
                        System.out.println("Unit added successfully.");
                        break;
                    case 's':
                        map.add(x, y, new Spikes(nextEntry.charAt(6)));
                        System.out.println("Spikes added successfully.");
                        break;
                    case 'b':
                        map.add(x, y, new Button(getControlled()));
                        System.out.println("Button added successfully.");
                        break;
                    case 'p':
                        map.add(x, y, new PushTile(getControlled()));
                        System.out.println("PushTile added successfully.");
                        break;
                    case 't':
                        map.add(x, y, new Target(getControlled(), nextEntry.charAt(6) == 't'));
                        System.out.println("Target added successfully.");
                        break;
                    case 'c':
                        map.add(x, y, new Cracked());
                        System.out.println("Cracked Unit added successfully.");
                        break;
                    case 'B':
                        map.add(x, y, new Block(nextEntry.charAt(6) == 't'));
                        System.out.println("Block added successfully.");
                        break;
                    case 'T':
                        char[] direction = new char[nextEntry.length() / 2 - 3];
                        for (int position = 8; position < nextEntry.length(); position += 2) {
                            direction[position / 2 - 4] = nextEntry.charAt(position);
                        }
                        map.add(x, y, new Turret(direction, nextEntry.charAt(6) == 't'));
                        System.out.println("Turret added successfully.");
                        break;
                    case 'r':
                        System.out.println(map.remove(x, y).toString() + " at (" + x + "," + y + ") has been removed");
                        break;
                    default:
                        System.out.println("Wrong entry, check it again.");
                }
            } else {
                switch (nextEntry.charAt(0)) {
                    case 'l':
                        System.out.println(map.getList());
                        System.out.println(COMPONENTSHORT);
                        break;
                    case 'm':
                        System.out.println(COMPONENTMENU);
                        break;
                    default:
                        System.out.println("Wrong entry, check it again.");
                }
            }
            nextEntry = scan.nextLine();
        }

        System.out.println(MAPSTARTMENU);
        nextEntry = scan.nextLine();
        int x = Character.getNumericValue(nextEntry.charAt(0));
        int y = Character.getNumericValue(nextEntry.charAt(2));
        map.setStart(x, y);

        System.out.println(MAPENDMENU);
        nextEntry = scan.nextLine();
        x = Character.getNumericValue(nextEntry.charAt(0));
        y = Character.getNumericValue(nextEntry.charAt(2));
        map.setEnd(x, y);

        scan.close();
        return map.complete();
    }

    /**
     * Gets Positions triggered a particular Component from the user.
     *
     * @return Positions controlled by the Component
     */
    private static Position[] getControlled() {
        return new Position[4];
    }


}
