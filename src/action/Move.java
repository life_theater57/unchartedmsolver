package action;

import java.util.LinkedList;

public class Move extends Action {
    private LinkedList<Character> steps;

    /**
     * Constructs a Move containing no step.
     */
    public Move() {
        steps = new LinkedList<Character>();
    }

    /**
     * Adds a step to the Move in direction specified.
     *
     * @param direction a char corresponding to direction of the step as below:
     *                  <ul>
     *                  <li>'N' : North</li>
     *                  <li>'E' : East</li>
     *                  <li>'S' : South</li>
     *                  <li>'W' : West</li>
     *                  </ul>
     */
    public void addStep(char direction) {
        if (direction == 'N' || direction == 'E' || direction == 'S' || direction == 'W') {
            steps.add(new Character(direction));
        }
    }

    /**
     * Adds a step at the beginning of the Move in direction specified.
     *
     * @param direction a char corresponding to direction of the step as below:
     *                  <ul>
     *                  <li>'N' : North</li>
     *                  <li>'E' : East</li>
     *                  <li>'S' : South</li>
     *                  <li>'W' : West</li>
     *                  </ul>
     */
    public void addFirstStep(char direction) {
        if (direction == 'N' || direction == 'E' || direction == 'S' || direction == 'W') {
            steps.addFirst(new Character(direction));
        }
    }

    /**
     * Remove the last step of the Move and return corresponding char.
     *
     * @return a char corresponding to direction of the step removed. Direction
     * are listed below:
     * <ul>
     * <li>'N' : North</li>
     * <li>'E' : East</li>
     * <li>'S' : South</li>
     * <li>'W' : West</li>
     * </ul>
     */
    public char removeStep() {
        return steps.removeLast();
    }

    /**
     * Returns the move length in number of step.
     *
     * @return number of step
     */
    public int lenght() {
        return steps.size();
    }

    @Override
    public String toString() {
        String list = "Move: ";
        for (Character step : steps) {
            list += step + " ";
        }
        return list;
    }
}
