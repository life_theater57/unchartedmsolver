package map;

public class Turret extends Unit {
    private boolean active, rotate;
    private int[] direction;
    private String activity;

    // input char are 'N','E', 'S' or 'W'
    public Turret(char[] direction, boolean rotate) {
        this.rotate = rotate;
        this.active = true;                             // default : active
        this.safe = false;
        this.bulletProof = true;                        // à vérifié
        this.direction = new int[direction.length];
        for (int i = 0; i < direction.length; i++) {
            switch (direction[i]) {
                case 'N':
                    this.direction[i] = 0;
                    break;
                case 'E':
                    this.direction[i] = 1;
                    break;
                case 'S':
                    this.direction[i] = 2;
                    break;
                case 'W':
                    this.direction[i] = 3;
                    break;
            }
        }
        this.activity = "0";
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @return the directions
     */
    public char[] getDirection() {
        char[] currentDirection = new char[direction.length];
        for (int i = 0; i < direction.length; i++) {
            switch (direction[i]) {
                case 0:
                    currentDirection[i] = 'N';
                    break;
                case 1:
                    currentDirection[i] = 'E';
                    break;
                case 2:
                    currentDirection[i] = 'S';
                    break;
                case 3:
                    currentDirection[i] = 'W';
                    break;
            }
        }
        return currentDirection;
    }

    @Override
    public String toString() {
        String string = "Turret direction\n  ";
        for (int i : direction) {
            switch (i) {
                case 0:
                    string += " N";
                    break;
                case 1:
                    string += " E";
                    break;
                case 2:
                    string += " S";
                    break;
                case 3:
                    string += " W";
                    break;
            }
        }
        return string;
    }

    @Override
    public void toggle() {
        active = !active;
    }

    @Override
    public boolean shot() {
        return true;
    }

    @Override
    public void next() {
        if (rotate && active) {
            for (int i = 0; i < direction.length; i++) {
                direction[i] = (direction[i] + 1) % 4;
            }
        }
        activity += (active) ? "1" : "0";
    }

    @Override
    public void previous() {
        active = (activity.substring(activity.length() - 1).equals("1")) ? true : false;
        activity = activity.substring(0, activity.length() - 1);
        if (rotate && active) {
            for (int i = 0; i < direction.length; i++) {
                direction[i] = (((direction[i] - 1) % 4) + 4) % 4;
            }
        }
    }
}
