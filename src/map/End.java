package map;

public class End extends Unit {
    Position[] required; //need to be active to open the end

    public End(Position[] required) {
        this.bulletProof = true; // vérifier si c'est vraiment le cas
        this.required = required;
    }

    /**
     * @return the required
     */
    public Position[] getRequired() {
        return required;
    }

    @Override
    public String toString() {
        String string = "End require:\n";
        for (Position i : required) {
            string += " " + i.toString();
        }
        return string + "\n";
    }

    @Override
    public void toggle() {
    }

    @Override
    public boolean shot() {
        return true;
    }
}
