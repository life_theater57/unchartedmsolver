package map;
public class TNT extends Carriable {
	/**
	 * Constructs a carriable TNT which explodes when shot.
	 */
	public TNT() {
		
	}
	
	@Override
	public String toString(){
		return "TNT";
	}
}
