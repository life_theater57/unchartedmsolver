package map;

import action.Action;
import action.Move;

import java.util.LinkedList;

public class Map {
    private int width, height, maxMoves;
    private Position start, end;
    private Unit[][] field;
    private boolean[][] safetyMap;
    private LinkedList<Turret> turrets;
    private LinkedList<Spikes> spikes;

    /**
     * Constructs a Map with width, height and a maximum number of Moves
     * specified.
     *
     * @param width    width of the Map to construct in number of case.
     * @param height   height of the Map to construct in number of case.
     * @param maxMoves maximum number of Moves allowed to solve the puzzle.
     */
    public Map(int width, int height, int maxMoves) {
        this.width = width;
        this.height = height;
        this.maxMoves = maxMoves;
        field = new Unit[width][height];
        safetyMap = new boolean[width][height];
        turrets = new LinkedList<Turret>();
        spikes = new LinkedList<Spikes>();
    }

    /**
     * Adds the Unit specified at specified position (x,y) to the Map.
     *
     * @param x         position North-South in number of cases of the Unit to add.
     * @param y         position West-East in number of cases of the Unit to add.
     * @param component the Unit to add to the Map.
     */
    public void add(int x, int y, Unit component) {
        if (field[x][y] == null) {
            field[x][y] = component;

            if (component instanceof Turret) {
                turrets.add((Turret) component);
            }

            if (component instanceof Spikes) {
                spikes.add((Spikes) component);
            }
        } else {
            throw new IllegalArgumentException("a Unit already exists at this position");
        }
    }

    /**
     * Adds the Carriable object specified at specified position (x,y) to the
     * Map.
     *
     * @param x      position West-East in number of cases of the Carriable
     *               object to add.
     * @param y      position North-South in number of cases of the Carriable object
     *               to add.
     * @param object Carriable object to add to the Map.
     */
    public void add(int x, int y, Carriable object) {
        if (field[x][y].isFree()) {
            field[x][y].put(object);
        } else {
            throw new IllegalArgumentException("a Carriable object already exists at this position");
        }
    }

    /**
     * Removes and returns Unit found at specified position (x,y) on the map.
     *
     * @param x position West-East in number of cases of the Unit to remove.
     * @param y position North-South in number of cases of the Unit to remove.
     * @return the Unit removed.
     */
    public Unit remove(int x, int y) {
        Unit component = field[x][y];
        if (component instanceof Turret) {
            turrets.remove(component);
        }

        if (component instanceof Spikes) {
            spikes.remove(component);
        }

        field[x][y] = null;

        return component;
    }

    /**
     * Returns an Action list to solve the puzzle from Start to End within maximum
     * number of Move.
     *
     * @return an Action list to solve the puzzle.
     */
    public LinkedList<Action> resolve() {
        LinkedList<Action> solution = new LinkedList<>();
        Move move = moveTo(end, start);
        if (move != null) {
            solution.add(move);
        }

        return solution;
    }

    /**
     * Sets the starting point of the puzzle at position (x,y) specified.
     *
     * @param x position West-East in number of cases of the starting point
     *          to set.
     * @param y position North-South in number of cases of the starting point to
     *          set.
     */
    public void setStart(int x, int y) {
        start = new Position(x, y); // vérifier validitée?
    }

    /**
     * Sets the End of the puzzle at position (x,y) specified.
     *
     * @param x position West-East in number of cases of the End to set.
     * @param y position North-South in number of cases of the End to set.
     */
    public void setEnd(int x, int y) {
        end = new Position(x, y);
    }

    /**
     * Returns the maximum number of moves allowed to solve the puzzle.
     *
     * @return the maximum number of moves.
     */
    public int getMaxMoves() {
        return maxMoves;
    }

    /**
     * Sets the maximum number of moves allowed to solve the puzzle to set.
     *
     * @param maxMoves the maximum number of moves to set.
     */
    public void setMaxMoves(int maxMoves) {
        this.maxMoves = maxMoves;
    }

    /**
     * Returns the width of the Map in number of case.
     *
     * @return the width of the Map.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the Map in number of case.
     *
     * @return the height of the Map.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns Position of the start.
     *
     * @return the start Position.
     */
    public Position getStart() {
        return start;
    }

    /**
     * Returns Position of the End.
     *
     * @return the end Position.
     */
    public Position getEnd() {
        return end;
    }

    /**
     * Returns the 2D-array field of Units.
     *
     * @return the field of Units.
     */
    public Unit[][] getField() {
        return field;
    }

    /**
     * Return a listing of all Units of the Map in a String.
     *
     * @return a listing of all Units of the Map
     */
    public String getList() {
        String list = "Units on the map are listed below.\n";
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (field[x][y] != null) {
                    list += field[x][y].toString() + " at (" + x + "," + y + ")\n";
                }
            }
        }
        return list + "\n";
    }

    @Override
    public String toString() {
        String safety = "Before update\n\n";
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                safety += safetyMap[i][j] + "\t";
            }
            safety += "\n";
        }
        safety += "\n\n";
        return safety;
    }

    /**
     * Updates the opening status of the End. It checks if all required
     * positions on the field are active and and set the End open if Units at
     * all required positions are active. Otherwise, it closes the End.
     */
    public void updateEnd() {
        field[end.getX()][end.getY()].setSafe(check(((End) field[end.getX()][end.getY()]).getRequired()));
    }

    /**
     * Checks if Units at all required position are active.
     *
     * @param required a list of all required positions.
     * @return true if all Units are active, false otherwise.
     */
    public boolean check(Position[] required) {
        return checkHelper(required, 0);
    }

    /**
     * Checks if current Unit in required positions is active and recursively
     * check if the Unit at the next position is active only if the current one
     * is active.
     *
     * @param required a list of all required positions.
     * @param i        index of the current position to check.
     * @return true if the Unit at current position and the next one are active,
     * false otherwise.
     */
    private boolean checkHelper(Position[] required, int i) {
        if (i < (required.length - 1)) {
            return ((Interactive) field[required[i].getX()][required[i].getY()]).isActive()
                    && checkHelper(required, i + 1);
        } else {
            return ((Interactive) field[required[i].getX()][required[i].getY()]).isActive();
        }
    }

    /**
     * Recursively called to create a Move going to a goal Position from a current
     * Position if there is such a safe path.
     *
     * @param goal    goal Position.
     * @param current current Position.
     * @return a Move from current to goal Position if there is such a safe path,
     * null otherwise.
     */
    private Move moveTo(Position goal, Position current) {
        if (current.distance(goal) == 1) {
            return new Move();
        }

        nextState();

        // move to the East direction
        if (current.getX() + 1 < width &&
                !field[current.getX() + 1][current.getY()].isPassed() &&
                safetyMap[current.getX() + 1][current.getY()]) {
            field[current.getX() + 1][current.getY()].setPassed(true);
            Move followingPath = moveTo(goal, new Position(current.getX() + 1, current.getY()));
            field[current.getX() + 1][current.getY()].setPassed(false);
            if (followingPath != null) {
                followingPath.addFirstStep('E');
                return followingPath;
            }
        }

        // move to the South direction
        if (current.getY() + 1 < height &&
                !field[current.getX()][current.getY() + 1].isPassed() &&
                safetyMap[current.getX()][current.getY() + 1]) {
            field[current.getX()][current.getY() + 1].setPassed(true);
            Move followingPath = moveTo(goal, new Position(current.getX(), current.getY() + 1));
            field[current.getX()][current.getY() + 1].setPassed(false);
            if (followingPath != null) {
                followingPath.addFirstStep('S');
                return followingPath;
            }
        }

        // move to the West direction
        if (current.getX() - 1 >= 0 &&
                !field[current.getX() - 1][current.getY()].isPassed() &&
                safetyMap[current.getX() - 1][current.getY()]) {
            field[current.getX() - 1][current.getY()].setPassed(true);
            Move followingPath = moveTo(goal, new Position(current.getX() - 1, current.getY()));
            field[current.getX() - 1][current.getY()].setPassed(false);
            if (followingPath != null) {
                followingPath.addFirstStep('W');
                return followingPath;
            }
        }

        // move to the North direction
        if (current.getY() - 1 >= 0 &&
                !field[current.getX()][current.getY() - 1].isPassed() &&
                safetyMap[current.getX()][current.getY() - 1]) {
            field[current.getX()][current.getY() - 1].setPassed(true);
            Move followingPath = moveTo(goal, new Position(current.getX(), current.getY() - 1));
            field[current.getX()][current.getY() - 1].setPassed(false);
            if (followingPath != null) {
                followingPath.addFirstStep('N');
                return followingPath;
            }
        }

        previousState();
        return null;
    }

    /**
     * Pushes each Unit one step forward. To achieve this, active Turrets shoot. If a target is shot,
     * it toggles its controlled Units. Then, each Unit is brought to its next state. Finally, the
     * safety map is updated.
     */
    private void nextState() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (field[i][j] instanceof Turret && ((Turret) field[i][j]).isActive()) {
                    for (char direction : ((Turret) field[i][j]).getDirection()) {
                        boolean hit = false;
                        switch (direction) {
                            case 'E':
                                for (int k = i + 1; k < width && !hit; k++) {
                                    if (field[k][j].shot()) {
                                        hit = true;
                                        if (field[k][j] instanceof Target) {
                                            for (Position p : ((Target) field[k][j]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'S':
                                for (int k = j + 1; k < height && !hit; k++) {
                                    if (field[i][k].shot()) {
                                        hit = true;
                                        if (field[i][k] instanceof Target) {
                                            for (Position p : ((Target) field[i][k]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'W':
                                for (int k = i - 1; k >= 0 && !hit; k--) {
                                    if (field[k][j].shot()) {
                                        hit = true;
                                        if (field[k][j] instanceof Target) {
                                            for (Position p : ((Target) field[k][j]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'N':
                                for (int k = j - 1; k >= 0 && !hit; k--) {
                                    if (field[i][k].shot()) {
                                        hit = true;
                                        if (field[i][k] instanceof Target) {
                                            for (Position p : ((Target) field[i][k]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
                field[i][j].next();
            }
        }
        updateSafetyMap();
    }

    /**
     * Pushes each Unit one step backward. To achieve this, each Unit is brought to its previous
     * state. Next, active Turrets shoot. If a target is shot, it toggles its controlled Units.
     * Then, the safety map is updated.
     */
    private void previousState() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                field[i][j].previous();
                if (field[i][j] instanceof Turret && ((Turret) field[i][j]).isActive()) {
                    for (char direction : ((Turret) field[i][j]).getDirection()) {
                        boolean hit = false;
                        switch (direction) {
                            case 'E':
                                for (int k = i + 1; k < width && !hit; k++) {
                                    if (field[k][j].shot()) {
                                        hit = true;
                                        if (field[k][j] instanceof Target) {
                                            for (Position p : ((Target) field[k][j]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'S':
                                for (int k = j + 1; k < height && !hit; k++) {
                                    if (field[i][k].shot()) {
                                        hit = true;
                                        if (field[i][k] instanceof Target) {
                                            for (Position p : ((Target) field[i][k]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'W':
                                for (int k = i - 1; k >= 0 && !hit; k--) {
                                    if (field[k][j].shot()) {
                                        hit = true;
                                        if (field[k][j] instanceof Target) {
                                            for (Position p : ((Target) field[k][j]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'N':
                                for (int k = j - 1; k >= 0 && !hit; k--) {
                                    if (field[i][k].shot()) {
                                        hit = true;
                                        if (field[i][k] instanceof Target) {
                                            for (Position p : ((Target) field[i][k]).getControlled()) {
                                                field[p.getX()][p.getY()].toggle();
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
        updateSafetyMap();
    }

    /**
     * Updates the safety map. To achieve this, it puts the safety state of each Unit of the
     * field on the safety map. Then, it puts unsafe state on safety map for each Unit between
     * active Turrets and the bulletproof object in each shooting direction.
     */
    private void updateSafetyMap() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                safetyMap[i][j] = field[i][j].isSafe();
            }
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (field[i][j] instanceof Turret && ((Turret) field[i][j]).isActive()) {
                    for (char direction : ((Turret) field[i][j]).getDirection()) {
                        boolean hit = false;
                        switch (direction) {
                            case 'E':
                                for (int k = i + 1; k < width && !hit; k++) {
                                    if (!field[k][j].shot()) {
                                        safetyMap[k][j] = false;
                                    } else {
                                        hit = true;
                                    }
                                }
                                break;
                            case 'S':
                                for (int k = j + 1; k < height && !hit; k++) {
                                    if (!field[i][k].shot()) {
                                        safetyMap[i][k] = false;
                                    } else {
                                        hit = true;
                                    }
                                }
                                break;
                            case 'W':
                                for (int k = i - 1; k >= 0 && !hit; k--) {
                                    if (!field[k][j].shot()) {
                                        safetyMap[k][j] = false;
                                    } else {
                                        hit = true;
                                    }
                                }
                                break;
                            case 'N':
                                for (int k = j - 1; k >= 0 && !hit; k--) {
                                    if (!field[i][k].shot()) {
                                        safetyMap[i][k] = false;
                                    } else {
                                        hit = true;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Completes the field to avoid a null pointer on the field. It adds an unsafe Unit
     * everywhere there is a null pointer on the field.
     *
     * @return the Map updated.
     */
    public Map complete() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (field[i][j] == null) {
                    field[i][j] = new Unit();
                    field[i][j].setSafe(false);
                }
            }
        }
        return this;
    }
}
