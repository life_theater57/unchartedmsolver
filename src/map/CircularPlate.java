package map;

public class CircularPlate extends Carriable {
	/**
	 * Constructs a carriable CircularPlate required to build a target.
	 */
	public CircularPlate() {

	}
	
	@Override
	public String toString() {
		return "CircularPlate";
	}
}
