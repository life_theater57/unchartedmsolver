package map;

public class Spikes extends Unit {
    private int state;

    //state:char {w - white, b - black, s - spikes}
    public Spikes(char state) {
        switch (state) {
            case 'w':
                this.state = 0;
                this.safe = true;
                break;
            case 'b':
                this.state = 1;
                this.safe = true;
                break;
            case 's':
                this.state = 2;
                this.safe = false;
                break;
        }
    }

    /**
     * @return the state
     */
    public int getState() {
        return state;
    }

    @Override
    public String toString() {
        String string = "Spikes state ";
        switch (state) {
            case 0:
                string += 'w';
                break;
            case 1:
                string += 'b';
                break;
            case 2:
                string += 's';
                break;
        }
        return string;
    }

    @Override
    public void toggle() {
    }

    @Override
    public void next() {
        state = (state + 1) % 3;
    }

    @Override
    public void previous() {
        state = (((state - 1) % 3) + 3) % 3;
    }
}
