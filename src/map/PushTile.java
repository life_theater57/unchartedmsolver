package map;
public class PushTile extends Interactive {
	private boolean pressed;
	
	public PushTile(Position[] controlled) {
		this.controlled = controlled;
		this.pressed = false;
	}
	
	/**
	 * @return the pressed
	 */
	public boolean isActive() {
		return pressed;
	}
	
	public String toString(){
		String string = "PushTile controlled:\n";
		for(Position i : controlled){
			string += " " + i.toString();
		}
		return string + "\n";
	}

	public void press() {
		pressed = true;
	}
	
	public void release() {
		pressed = false;
	}
}
