package map;

public class Unit {
    protected boolean safe;
    protected boolean passed;
    protected boolean bulletProof;

    protected Carriable object;
    private Position controller;

    /**
     * Constructs a simple Unit.
     */
    public Unit() {
        this.safe = true;
        this.passed = false;
        this.bulletProof = false;
    }

    /**
     * Constructs a Unit with the Position of a Controller.
     *
     * @param controller Position of the controller of the Unit.
     */
    public Unit(Position controller) {
        this.safe = true;
        this.passed = false;
        this.bulletProof = false;
        this.controller = controller;
    }

    /**
     * Returns the safety state of the Unit.
     *
     * @return true if the Unit is safe, false otherwise.
     */
    public boolean isSafe() {
        return this.safe;
    }

    /**
     * Sets the safety state of the Unit.
     *
     * @param safe the safety state to set
     */
    public void setSafe(boolean safe) {
        this.safe = safe;
    }

    /**
     * Returns the controller Position of the Unit.
     *
     * @return the Position of the controller.
     */
    public Position getController() {
        return controller;
    }

    /**
     * Sets the controller Position of the Unit.
     *
     * @param controller the controller Position to set
     */
    public void setController(Position controller) {
        this.controller = controller;
    }

    /**
     * Returns whether the Unit is bulletproof or not.
     *
     * @return true if the Unit is bulletproof, false otherwise.
     */
    public boolean isBulletProof() {
        return bulletProof;
    }

    /**
     * Returns whether the Unit is free or not.
     *
     * @return true if the Unit is free, false otherwise.
     */
    public boolean isFree() {
        return object == null;
    }

    /**
     * Returns whether the Move already passed on the Unit.
     *
     * @return true if the Move already passed on the Unit, false otherwise.
     */
    public boolean isPassed() {
        return passed;
    }

    /**
     * Sets the passed boolean according to the path of the Move.
     *
     * @param passed the passed boolean to set.
     */
    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    /**
     * Adds a Carriable object to the Unit.
     *
     * @param object the object to add.
     */
    public void put(Carriable object) {
        this.object = object;
    }

    /**
     * Removes the Carriable object from the Unit and returns it.
     *
     * @return the object removed from the Unit.
     */
    public Carriable take() {
        Carriable temp = object;
        object = null;
        return temp;
    }

    @Override
    public String toString() {
        return "Unit";
    }

    /**
     * Called when a Character pushes on the Unit.
     */
    public void push() {
    }

    /**
     * Called when a Character or a Carriable object presses on the Unit.
     */
    public void press() {
    }

    /**
     * Called when a Character or a Carriable object exits the Unit.
     */
    public void release() {
    }

    /**
     * Called when a Character or a Turret shoots on the Unit.
     *
     * @return return true when the Unit has been hit by the bullet, false otherwise.
     */
    public boolean shot() {
        return false;
    }

    /**
     * Toggles the safety state of the Unit.
     */
    public void toggle() {
        safe = !safe;
    }

    /**
     * Push the Unit to its next state
     */
    public void next() {
    }

    ;

    /**
     * Push the Unit to its previous state
     */
    public void previous() {
    }

    ;
}
