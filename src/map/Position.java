package map;

public class Position {
    private int x, y;

    /**
     * Constructs a default position at (0,0).
     */
    public Position() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Constructs a position with x, y coordinates specified.
     *
     * @param x West-East position
     * @param y North-South position
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns West-East position.
     *
     * @return x value.
     */
    public int getX() {
        return this.x;
    }

    /**
     * Sets West-East position and returns new Position.
     *
     * @param x value to set.
     * @return Position with new x value.
     */
    public Position setX(int x) {
        this.x = x;
        return this;
    }

    /**
     * Returns North-South position.
     *
     * @return y value
     */
    public int getY() {
        return this.y;
    }

    /**
     * Sets North-South position and returns new Position.
     *
     * @param y value to set.
     * @return Position with new y value.
     */
    public Position setY(int y) {
        this.y = y;
        return this;
    }

    /**
     * Returns a String representation of the Position as (x,y).
     *
     * @return a String representation of the Position.
     */
    public String toString() {
        String position = "(" + this.x + "," + this.y + ")";
        return position;
    }

    /**
     * Returns distance between this Position and the one specified in number of
     * case.
     *
     * @param p1 one of the 2 positions.
     * @return distance in number of case between positions.
     */
    public int distance(Position p1) {
        int distance = Math.abs(p1.getX() - this.getX()) + Math.abs(p1.getY() - this.getY());
        return distance;
    }

    /**
     * Returns distance between 2 Positions specified in number of case.
     *
     * @param p1 first Position
     * @param p2 second Position
     * @return distance in number of case between positions.
     */
    public static int distance(Position p1, Position p2) {
        int distance = Math.abs(p1.getX() - p2.getX()) + Math.abs(p1.getY() - p2.getY());
        return distance;
    }
}
