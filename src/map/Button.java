package map;
public class Button extends Interactive {
	private boolean pushed;
	
	public Button(Position[] controlled) {
		this.safe = false;
		this.controlled = controlled;
		this.pushed = false;
	}
	
	/**
	 * @return the pushed
	 */
	public boolean isActive() {
		return pushed;
	}
	
	public String toString(){
		String string = "Button controlled:\n";
		for(Position i : controlled){
			string += " " + i.toString();
		}
		return string + "\n";
	}
	
	public void push() {
		pushed = !pushed;
	}
}
