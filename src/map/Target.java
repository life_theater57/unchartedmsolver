package map;

public class Target extends Interactive {
    private boolean built;
    private boolean shot;

    public Target(Position[] controlled, boolean built) {
        this.safe = false;
        this.passed = false;
        this.bulletProof = true;
        this.controlled = controlled;
        this.built = built;
        this.shot = false;
    }

    /**
     * @return the built
     */
    public boolean isBuilt() {
        return built;
    }

    /**
     * @return the shot
     */
    public boolean isActive() {
        return shot;
    }

    @Override
    public String toString() {
        return "Target";
    }

    @Override
    public boolean shot() {
        if (built) {
            shot = !shot;
        }
        return true;
    }

    /**
     * Constructs the Target from an Target not built and a CircularPlate.
     */
    public void contruct() {
        built = true;
    }
}
