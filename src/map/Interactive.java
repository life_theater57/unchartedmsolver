package map;
public abstract class Interactive extends Unit {
	abstract boolean isActive(); 
	protected Position[] controlled;
	
	/**
	 * @return the controlled
	 */
	public Position[] getControlled() {
		return controlled;
	}
	
	public void toggle() {}
}
