package map;

/**
 * It includes static rocks.
 */
public class Block extends Unit {
    /**
     * Constructs a Block with safety state specified and the bulletproof property corresponding.
     *
     * @param safe the safety state
     */
    public Block(boolean safe) {
        this.safe = safe;
        this.bulletProof = !safe;
    }

    @Override
    public String toString() {
        return "Block";
    }

    /**
     * Toggles safety state and bulletproof property.
     */
    public void toggle() {
        safe = !safe;
        bulletProof = !bulletProof;
    }

    @Override
    public boolean shot() {
        return bulletProof;
    }
}
