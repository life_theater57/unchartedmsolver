This program will solve UNCHARTED: Fortune Hunter™ problems or any game using the same structure.


Langage: Java -- 
UI: command line


Already done:

* All classes to represent in-game objects

* Move Action class

* Solving puzzle that requires only a single Move


TODO:

* Add Shoot, Take, PutDown, Pull, Push and Build to Action classes

* Continue the algorithm to solve puzzles and implement it


Compiling from unchartedMSolver folder: 
    
```
#!bash

javac -sourcepath src -classpath bin -d bin src/Main.java
```

Executing from unchartedMSolver folder: 
    
```
#!bash

java -classpath bin Main
```

*input_instructions.txt* file is in doc folder. 
*input_example.txt* is also in doc folder.